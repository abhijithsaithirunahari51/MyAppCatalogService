package com.myapp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

import com.myapp.Repository.ItemRepository;
import com.myapp.domain.Item;

@SpringBootApplication
@ComponentScan
@EnableDiscoveryClient
@Component
public class MyAppCatalogApplication {
	
	private final ItemRepository itemRepository;
	
	@Autowired
	public MyAppCatalogApplication(ItemRepository itemRepository) {
		this.itemRepository=itemRepository;
	}
	
	public void generateTestData() {
		itemRepository.save(new Item("ipad",42.0));
		itemRepository.save(new Item("ipad touch",21.0));
		itemRepository.save(new Item("ipad nano",1.0));
		itemRepository.save(new Item("Apple TV",100.0));
	}
	
	public static void main(String[] args) {
		SpringApplication.run(MyAppCatalogApplication.class, args);
	}
}
