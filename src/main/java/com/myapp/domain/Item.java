package com.myapp.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
@Entity
public class Item {
	@Id
	@GeneratedValue
	private long id;
	
	@Column(nullable=false)
	private String name;
	@Column(nullable=false)
	private double price;
	
	public Item() {
		super();
		id=01;
	}
	public Item(String name, double price) {
		this.name=name;
		this.price=price;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	
}
